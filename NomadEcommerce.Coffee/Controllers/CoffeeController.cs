using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using NomadEcommerce.Coffee.DataContext;
using NomadEcommerce.Coffee.Models;

namespace NomadEcommerce.Coffee.Controllers
{
    public class CoffeeController : Controller
    {
        private readonly CoffeeDBContext _context;

        public CoffeeController(CoffeeDBContext context)
        {
            _context = context;
        }

        // GET: Coffee
        public async Task<IActionResult> Index()
        {
            return View(await _context.CoffeeModels.Include(x => x.Images).ToListAsync());
        }

        // GET: Coffee/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var coffeeModel = await _context.CoffeeModels.Include(x=>x.Images)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (coffeeModel == null)
            {
                return NotFound();
            }

            return View(coffeeModel);
        }

        // GET: Coffee/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Coffee/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,Type,ImageFile")] CoffeeModel coffeeModel)
        {
            if (ModelState.IsValid)
            {
                if (coffeeModel.ImageFile != null && coffeeModel.ImageFile.Length > 0)
                {
                    var extension = Path.GetExtension(coffeeModel.ImageFile?.FileName);
                    var size = coffeeModel.ImageFile?.Length;
                    if (!extension.ToLower().Equals(".png"))
                    {
                        ModelState.AddModelError("ImageFile", "Only png files are allowed");
                        return View(coffeeModel);
                    }

                    if (size > (50 * 1024))
                    {
                        ModelState.AddModelError("ImageFile", "File size is bigger than 50 KB.");
                        return View(coffeeModel);
                    }

                    using (var memoryStream = new MemoryStream())
                    {
                        await coffeeModel.ImageFile.CopyToAsync(memoryStream);
                        byte[] bytes = memoryStream.ToArray();
                        string base64String = Convert.ToBase64String(bytes, 0, bytes.Length);
                        coffeeModel.Images = new List<CoffeeImageModel>
                        {
                            new CoffeeImageModel { Data = "data:image/png;base64," + base64String }
                        };
                    }
                }

                _context.Add(coffeeModel);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(coffeeModel);
        }

        // GET: Coffee/Edit/5
        [Authorize]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var coffeeModel = await _context.CoffeeModels.FindAsync(id);
            if (coffeeModel == null)
            {
                return NotFound();
            }
            return View(coffeeModel);
        }

        // POST: Coffee/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Type,Replace,ImageFile")] CoffeeModel coffeeModel)
        {
            if (id != coffeeModel.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(coffeeModel);
                    await _context.SaveChangesAsync();
                    if (coffeeModel.ImageFile != null && coffeeModel.ImageFile.Length > 0)
                    {
                        var extension = Path.GetExtension(coffeeModel.ImageFile?.FileName);
                        var size = coffeeModel.ImageFile?.Length;
                        if (!extension.ToLower().Equals(".png"))
                        {
                            ModelState.AddModelError("ImageFile", "Only png files are allowed");
                            return View(coffeeModel);
                        }

                        if (size > (50 * 1024))
                        {
                            ModelState.AddModelError("ImageFile", "File size is bigger than 50 KB.");
                            return View(coffeeModel);
                        }

                        using (var memoryStream = new MemoryStream())
                        {
                            await coffeeModel.ImageFile.CopyToAsync(memoryStream);
                            byte[] bytes = memoryStream.ToArray();
                            string base64String = Convert.ToBase64String(bytes, 0, bytes.Length);
                            var existing = await _context.CoffeeModels.Include(x => x.Images).FirstOrDefaultAsync(x => x.Id == id);
                            if (coffeeModel.Replace)
                            {
                                existing.Images.Clear();
                                await _context.SaveChangesAsync();
                            }
                            existing.Images.Add(new CoffeeImageModel { Data = "data:image/png;base64," + base64String });
                            await _context.SaveChangesAsync();
                        }
                    }
                    
                    
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CoffeeModelExists(coffeeModel.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(coffeeModel);
        }

        // GET: Coffee/Delete/5
        [Authorize]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var coffeeModel = await _context.CoffeeModels
                .FirstOrDefaultAsync(m => m.Id == id);
            if (coffeeModel == null)
            {
                return NotFound();
            }

            return View(coffeeModel);
        }

        // POST: Coffee/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var coffeeModel = await _context.CoffeeModels.FindAsync(id);
            _context.CoffeeModels.Remove(coffeeModel);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CoffeeModelExists(int id)
        {
            return _context.CoffeeModels.Any(e => e.Id == id);
        }
    }
}
