﻿using Microsoft.EntityFrameworkCore;
using NomadEcommerce.Coffee.Models;

namespace NomadEcommerce.Coffee.DataContext
{
    public class CoffeeDBContext : DbContext
    {
        public CoffeeDBContext(DbContextOptions<CoffeeDBContext> options)
            : base(options)
        {
        }

        public DbSet<CoffeeModel> CoffeeModels { get; set; }

        public DbSet<CoffeeImageModel> CoffeeImageModels { get; set; }
    }
}
