﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NomadEcommerce.Coffee.Models
{
    public class CoffeeModel
    {
        public int Id { get; set; }
        public string Name { get; set; }

        [EnumDataType(typeof(CoffeeType))]
        public CoffeeType Type { get; set; }
        public List<CoffeeImageModel> Images { get; set; }

        [NotMapped]
        [DisplayName("Replace Existing Images")]
        public bool Replace { get; set; }

        [NotMapped]
        [DisplayName("Upload Image File")]
        public IFormFile ImageFile { get; set; }
    }
}
