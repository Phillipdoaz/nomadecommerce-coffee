﻿namespace NomadEcommerce.Coffee.Models
{
    public enum CoffeeType
    {
        OriginalBlend = 1,
        LightRoast,
        MediumRoast,
        DarkRoast,
        FrenchRoast,
        DecafLightRoast,
        DecafMediumRoast,
        DecafDarkRoast,
        DecafFrenchRoast,
        DecafBreakfastBlend
    }
}
