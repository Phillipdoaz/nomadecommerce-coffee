﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace NomadEcommerce.Coffee.Models
{
    public class CoffeeImageModel
    {
        public int Id { get; set; }
        public string Data { get; set; }

    }
}

